# Propuesta de solucion de oferta de productos financieros

## Diagrama de arquitectura
![Diagrama 1!](./assets/ArchDiagram.drawio.png "Comprension actual")
---
---
![Diagrama 2!](./assets/ArchDiagram_OnPremise.drawio.png "Comprension actual")

## Stack de tecnologias

1. Azure Cloud Services
2. Azure Container Instances
3. Azure Web App
4. Azure AD
5. Cosmo DB
6. Azure Container Registry
7. Azure Monitor
8. Azure Monitoring
9. Azure Storage : File Shared Service
10. [Azure Database Migration?] (https://azure.microsoft.com/en-us/pricing/details/database-migration/)

> Solucion basada en contenedores
> > Tipo de contenedores es linux
>
> > Componentes genericos Spring Boot
>
> > Componentes con interaccion con productos Microsoft en NetCore
>
> > Front End ReactJS
>
> > Elasticsearch para manejar Logs y eventos de auditoria
>
> > AMQP RabbitMQ

**Dato importante**
[AKS - ACI](https://docs.microsoft.com/es-es/azure/architecture/solution-ideas/articles/scale-using-aks-with-aci)

***La escala de esta arquitectura es vertical***

## Pipelie
1- [SCA](https://www.sonarqube.org/)

2- [Component SCAN](https://owasp.org/www-project-dependency-check/)

3- [Docker Scan](https://docs.docker.com/engine/scan/)

3- Analisis de seguridad
-   [OWASP ZAP](https://www.zaproxy.org/)
-   [OWASP SKF](https://owasp.org/www-project-security-knowledge-framework/)
-   [Defect Dojo](https://owasp.org/www-project-defectdojo/)

4 - [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/reference-index?view=azure-cli-latest)

## Protocolos de comunicacion entre servicios
1. REST Json
2. [Maturity Model / Glory of rest communications](https://martinfowler.com/articles/richardsonMaturityModel.html)
3. [AD OAUTH2](https://docs.microsoft.com/en-us/azure/active-directory/fundamentals/auth-oauth2)

## Patrones aparentes y diagrama tecnico

1. [CQRS](https://docs.microsoft.com/en-us/azure/architecture/patterns/cqrs)
2. [Event Driven Arch](https://martinfowler.com/articles/201701-event-driven.html)
3. GraphQL Impl

> Diagrama de general de interaccion
![Diagrama 1!](./assets/Components.drawio.png "Comprension actual")

## Integración de subsistemas

En base al Roadmap, es considerable pagar el costo de migracion de datos de los sistemas en on premise

### Estrategia

> Research sobre Azure Database Migration Service

Alternativa

> Construir un Custom Database Migration Service

![Diagrama 1!](./assets/IntegrationSystem.drawio.png "Comprension actual")