# Tratamiento de datos

## Datos de aplicacion
Este apartado se especifica la razon de la seleccion del gestor de datos de la aplicación, brindando razones justificadas en base a experiencia y documentacion tecnica

## Contexto de datos

> Estructura de datos del producto financiero

1-  [Productos financieros Cuscatlan SV](https://www.bancocuscatlan.com/cuscatlan-oro/privilegios/).
* [Cuenta corriente](https://www.bancocuscatlan.com/cuscatlan-oro/privilegios/cuenta-corriente).
* [Prestamo](https://www.bancocuscatlan.com/cuscatlan-oro/privilegios/prestamos-personales).

* [Tarjeta de credito](https://www.bancocuscatlan.com/cuscatlan-oro/privilegios/tarjetas-de-credito).

2- [CityBank GT Tarjetas de credito](https://www.citibank.com/icg/sa/latam/guatemala/cash-management/commercial-cards.html)
* [CityBank GT Prestamos personales](https://www.citibank.com/icg/sa/latam/guatemala/foreign-trade/credit-approval.html)

**Estructura de datos es diferente entre tipos de productos y paises**

> Estructura de datos de clientes

Datos de ciudadanos tiende a cambiar

* Identificaciones
* Telefono
* Direcciones

**Estructura de datos de clientes es diferente**


## Teorema de CAP (Analisis simple)

**BASE de datos tipo AP**

> La escalabilidad horizontal por atributos de precio y crecimiento de estructura de datos

> Tipo de bases de datos suele tener como principal caracteristicas disponibilidad

> Velocidad de procesamiento de datos

> Trasabilidad de datos es baja prioridad

## Logs

* [Sentry](https://www.thoughtworks.com/es-es/radar/tools/sentry)
* [Elasticsearch](https://www.elastic.co/es/logstash/)