# Roadmap

## Criterios IT
1. Alta Disponibilidad
2. Escalabilidad
3. Segura
4. Flexible
5. Rendimiento

## Restricciones
1. Solución basada en la nube
2. Costos optimos a la solucion
3. Flexible en integracion de sub-sistemas

## Elementos no funcionales tomados en cuenta
1. Restricciones de politicas nacionales tales como tratamiento de datos
2. Restricciones de productos por entidades que rigen economias nacionales

## Impacto de negocio y consideraciones posibles

> Datos entre cada subsistemas puede estar en diferente gestores y organizados en diferentes modelos de datos

> Sistemas pueden estar basados en tecnologias ya no soportadas

> Usuarios pueden resistir cambio de tecnologias o etapas de aprendizaje

![Diagrama 1!](../assets/Roadmap.drawio.png "Comprension actual")
